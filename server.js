let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let users = []
let messages = []
let index = 0
let id = 0
let adm = ['Marcus','Peter']

let conversas = [{
    idUser: 0,
    idAdm: 0,
    msgs:[]
}]

let admSelected;


/* app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
}); */


http.listen(3000, () => {
    console.log('Listening on port *: 3000');
});

io.on('connection', (socket) => {

   /*  socket.on('admSelected',data =>{
        conversas = data
        
    }) */

    socket.emit('loggedIn', {
        users: users.map(s => s.username),
        messages: messages,
        //id : id,
    })

    //socket.broadcast.emit('connection', Object.keys(io.sockets.connected).length);
    
    socket.on('disconnect', function(){
        //console.log('user disconnected');
        console.log(`${socket.username} has left the party`)
        
        io.emit('userLeft', socket.username)
        users.splice(users.indexOf(socket), 1)
    });

    //console.log(socket)

    socket.on('newuser', username => {
        
        console.log(`${username} has arrived at the party`)
        socket.username = username
        users.push(socket)
        
        io.emit('userOnline', socket.username)

    })

    socket.on('msg', msg => {
        let message = {
            index: index,
            id: id,
            username: socket.username,
            msg: msg,
            //type: type
        } 
 
        messages.push(message)
 
        io.emit('msg', message)
 
        id++
        index++
        //type++
 
     }) 

     

        /*
            socket.on('chat-message', (data) => {
                socket.broadcast.emit('chat-message', (data));
                console.log(data)
            });    
         
            socket.on('typing', (data) => {
                socket.broadcast.emit('typing', (data));
            });

            socket.on('stopTyping', () => {
                socket.broadcast.emit('stopTyping');
            });

            socket.on('joined', (data) => {
                socket.broadcast.emit('joined', (data));
            });

            socket.on('leave', (data) => {
                socket.broadcast.emit('leave', (data));
            });
        */
});